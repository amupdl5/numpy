{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1 2 3]\n"
     ]
    }
   ],
   "source": [
    "import numpy as np\n",
    "A=np.array([1,2,3])\n",
    "print(A)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[1 2 3]\n",
      " [4 5 6]]\n"
     ]
    }
   ],
   "source": [
    "import numpy as np\n",
    "A=np.array([(1,2,3),(4,5,6)])\n",
    "print(A)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1 2 3]\n",
      "[[1 2 3]\n",
      " [4 5 6]]\n",
      "[[1 2 3]\n",
      " [4 5 6]\n",
      " [7 8 9]]\n"
     ]
    }
   ],
   "source": [
    "import numpy as np\n",
    "a = np.array([1,2,3]) # single dimension array\n",
    "a2 = np.array([(1,2,3),(4,5,6)]) # 2-d array\n",
    "a3 = np.array([(1,2,3),(4,5,6),(7,8,9)]) #  3- dimension array\n",
    "print(a)\n",
    "print(a2)\n",
    "print(a3)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "2\n"
     ]
    }
   ],
   "source": [
    "#dimension of array\n",
    "\n",
    "import numpy as np\n",
    "A=np.array([[1,2,3],[4,5,6],[7,8,9]])\n",
    "print(A.ndim)\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "collapsed": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "4 bytes\n"
     ]
    }
   ],
   "source": [
    "#to find size\n",
    "import numpy as np\n",
    "A = np.array([[1,2,3],[4,5,6],[7,8,9]])\n",
    "\n",
    "print (A.itemsize,\"bytes\")\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "int32\n"
     ]
    }
   ],
   "source": [
    "#to find datatype\n",
    "import numpy as np\n",
    "A = np.array([[1,2,3],[4,5,6],[7,8,9]])\n",
    "\n",
    "print (A.dtype)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "original array\n",
      "[[1 2]\n",
      " [3 4]\n",
      " [5 6]]\n",
      "reshaping 3,2 array into 2,3\n",
      "[[1 2 3]\n",
      " [4 5 6]]\n"
     ]
    }
   ],
   "source": [
    "#reshapping the array\n",
    "import numpy as np\n",
    "a = np.array([[1,2],[3,4],[5,6]])\n",
    "print(\"original array\")\n",
    "print (a)\n",
    "\n",
    "a = a.reshape(2,3)\n",
    "print(\"reshaping 3,2 array into 2,3\")\n",
    "print(a)\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "2\n",
      "5\n"
     ]
    }
   ],
   "source": [
    "#reshapping the array\n",
    "import numpy as np\n",
    "\n",
    "a = np.array([[1,2],[3,4],[5,6]])\n",
    "print (a[0,1])\n",
    "print (a[2,0])\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[ 5.          6.11111111  7.22222222  8.33333333  9.44444444 10.55555556\n",
      " 11.66666667 12.77777778 13.88888889 15.        ]\n"
     ]
    }
   ],
   "source": [
    "#line spacing\n",
    "import numpy as np\n",
    "A=np.linspace(5,15,10)\n",
    "print(A)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "the maximum value 8\n",
      "2\n",
      "24\n"
     ]
    }
   ],
   "source": [
    "import numpy as np\n",
    "a=np.array([2,3,5,6,8])\n",
    "print(\"the maximum value\",a.max())\n",
    "print(a.min())\n",
    "print(a.sum())\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The array: [[2 3 4]\n",
      " [5 6 7]]\n",
      "The maximum elements of columns: [5 6 7]\n",
      "The minimum element of rows: [2 5]\n",
      "The sum of all rows: [ 9 18]\n"
     ]
    }
   ],
   "source": [
    "import numpy as np\n",
    "a=np.array([[2,3,4],[5,6,7]])\n",
    "print(\"The array:\",a)\n",
    "print(\"The maximum elements of columns:\",a.max(axis = 0))   \n",
    "print(\"The minimum element of rows:\",a.min(axis = 1))  \n",
    "print(\"The sum of all rows:\",a.sum(axis = 1))  \n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 30,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Sum of array a and b\n",
      " [[ 2  4 33]\n",
      " [22 34 33]]\n",
      "Product of array a and b\n",
      " [[  1   4  90]\n",
      " [120 285 116]]\n",
      "Division of array a and b\n",
      " [[ 1.          1.         10.        ]\n",
      " [ 0.83333333  0.78947368  0.13793103]]\n"
     ]
    }
   ],
   "source": [
    "#airthematic opetaion in array\n",
    "import numpy as np  \n",
    "a = np.array([[1,2,30],[10,15,4]])  \n",
    "b = np.array([[1,2,3],[12, 19, 29]])  \n",
    "print(\"Sum of array a and b\\n\",a+b)  \n",
    "print(\"Product of array a and b\\n\",a*b)  \n",
    "print(\"Division of array a and b\\n\",a/b)\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 31,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Arrays vertically concatenated\n",
      " [[ 1  2 30]\n",
      " [10 15  4]\n",
      " [ 1  2  3]\n",
      " [12 19 29]]\n",
      "Arrays horizontally concatenated\n",
      " [[ 1  2 30  1  2  3]\n",
      " [10 15  4 12 19 29]]\n"
     ]
    }
   ],
   "source": [
    "#adding the array thats called concatenation\n",
    "import numpy as np  \n",
    "a = np.array([[1,2,30],[10,15,4]])  \n",
    "b = np.array([[1,2,3],[12, 19, 29]])  \n",
    "print(\"Arrays vertically concatenated\\n\",np.vstack((a,b))); #vertical\n",
    "print(\"Arrays horizontally concatenated\\n\",np.hstack((a,b))) #horizontal \n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 36,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[0 3 6 9]\n"
     ]
    }
   ],
   "source": [
    "#range in array\n",
    "import numpy as np\n",
    "a= np.arange(0,10,3,dtype=int)\n",
    "print(a)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 37,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[0. 1. 2. 3.]\n"
     ]
    }
   ],
   "source": [
    "import numpy as np\n",
    "a=np.arange(4.0)\n",
    "print(a)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 39,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1 4 7]\n"
     ]
    }
   ],
   "source": [
    "import numpy as np\n",
    "a=np.arange(1,10,3)\n",
    "print(a)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 40,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1. 4. 7.]\n"
     ]
    }
   ],
   "source": [
    "import numpy as np\n",
    "a=np.arange(1,10,3,dtype=float)\n",
    "print(a)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
